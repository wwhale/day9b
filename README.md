# README #

# STEPS for snake app#

* create app/snake.module.js as below
* define express
* var express = require("express");
* var app = express();
* define port
* const NODE_PORT = process.env.PORT || 3000;
* app.listen(NODE_PORT, function () {
*    console.log("Web App started at " + NODE_PORT);
* });
* define path
* app.use(express.static(__dirname + "/../client/"));
* make the app public. 
* module.exports = app

* update snake.html as below
* include css, bower components, js scripts
*    <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css" />
*    <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap-theme.css" />
*    <link rel="stylesheet" type="text/css" href="css/snake.css">

*    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
*    <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

*    <script src="app/snake.module.js"></script> 
*    <script src="app/snake.controller.js"></script> 

* include angular app
* <html ng-app='ngSnake'>
* include angular controller
* <div id="gameContainer" ng-controller="snakeCtrl as ctrl">
* link angular variables
* {{ctrl.score}} ng-repeat="column in ctrl.board" ng-style="{'background-color': ctrl.setStyling($parent.$index, $index)}" ng-click="ctrl.startGame()"

* update snake.controller.js
* update controller
* (function() { })();
* .controller('snakeCtrl', snakeCtrl);
* update inject
* snakeCtrl.$inject=["$timeout", "$window"];
* update controller function
* function snakeCtrl ($timeout, $window) {
* define global scope
* var self = this;
* define global functions (examples)
* self.snakeCtrl = snakeCtrl;
* self.setStyling = setStyling;
* self.update = update;
* update variable to global (examples)
* self.BOARD_SIZE = 20;
* update functions and variables

* To run nodemon#
* cd to folder
* nodemon
* node "filename".js

#to test#
* http://localhost:3000 

# STEPS for setup#

#Setup Bitbucket#
* create respo
* Choose public or private repo access level
* create readme.md

#Prepare project structure#
* create folder 
* mkdir <project name>
* mkdir <project name>/server
* mkdir <project name>/client
* new file: /.gitignore
* new file: /.bowerrc

#Prepare Git#

* (To config)
* git init
* add remote origin , git remote add origin <http git url>. e.g. git remote add origin https://wwhale@bitbucket.org/wwhale/day5wa.git
* Create a git ignore file .gitignore e.g. node_modules

* git remote -v (check on the current directory which remote is configured to)
* To remove the git origin remote ( git remote remove origin)

* (To update)
* git add .
* git commit -m "<message>"
* git push orign master

* git pull origin master
* git clone origin master



#Global Utility installation#

* (node.js)
* cd to the working folder 
* npm init
* -- entry point
* -- path from bit bucket
* npm install -g nodemon
* npm install


* (express.js)
* cd to the working folder 
* npm install –save express
* npm install
* -- node_modules folder created

* (bower.js)
* .bowerrc file create
* -- "directory": "client/bower_components"
* bower init
* npm install -g bower
* npm i bower –g
* bower install bootstrap font-awesome angular --save

* (body-parser)
* npm install body-parser --save

* (heroku)
* install heroku
* heroku login
* heroku create
* git remote add heroku https://git.heroku.com/murmuring-cove-50493.git
* git remote -v
* git add .
* git commit –m “message”* 
* git push heroku master -u


#To run nodemon#
* cd to folder
* nodemon
* node "filename".js

#to test#
* http://localhost:4000 or https://murmuring-cove-50493.herokuapp.com/

 app#

# STEPS for calculator app#

* create server/app.js 
* define express(), bodyParser(), PORT, json

* create app/app.module.js
* define module calculator

* create app/appmain.controller.js
* define angular controller MainCtrl
* create function MainCtrl

* create app/appcalculator.js
* define angular controller CalculatorCtrl
* create function CalculatorCtrl

* create app/app.css
* create css class

* create index.html
* define ng-app, link css, define ng-controller, ng-include, link app/app.module.js, app/appmain.controller.js, app/appcalculator.controller.js

* create views/home.html
* create views/calculator.html

* cd to folder
* run nodemon
* open chrome, goto localhost:3000

* Update to git
* git add .
* git commit -m "<message>"
* git push orign master

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact