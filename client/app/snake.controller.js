(function() { 

  angular.module('ngSnake').controller('snakeCtrl', snakeCtrl);

  snakeCtrl.$inject=["$timeout", "$window"];

  function snakeCtrl ($timeout, $window) {
      var self = this;

      self.snakeCtrl = snakeCtrl;
      self.setStyling = setStyling;
      self.update = update;
      self.getNewHead = getNewHead;
      self.boardCollision = boardCollision;
      self.selfCollision = selfCollision;
      self.fruitCollision = fruitCollision;
      self.resetFruit = resetFruit;
      self.eatFruit = eatFruit;
      self.gameOver = gameOver;
      self.setupBoard = setupBoard;

     
      self.BOARD_SIZE = 20;

      self.DIRECTIONS = {

        LEFT: 37,

        UP: 38,

        RIGHT: 39,

        DOWN: 40

      };



      self.COLORS = {

        GAME_OVER: '#820303',

        FRUIT: '#E80505',

        SNAKE_HEAD: '#078F00',

        SNAKE_BODY: '#0DFF00',

        
        BOARD: '#000000'

      };



      self.snake = {

        direction: self.DIRECTIONS.LEFT,

        parts: [{

          x: -1,

          y: -1

        }]

      };



      self.fruit = {

        x: -1,

        y: -1

      };



      self.interval, self.tempDirection, self.isGameOver;


      self.score = 0;
      //$scope.score = 0;



      function setStyling(col, row) {
      
        if (self.isGameOver)  {

          return self.COLORS.GAME_OVER;

        } else if (self.fruit.x == row && self.fruit.y == col) {

          return self.COLORS.FRUIT;

        } else if (self.snake.parts[0].x == row && self.snake.parts[0].y == col) {

          return self.COLORS.SNAKE_HEAD;

        } else if (self.board[col][row] === true) {

          return self.COLORS.SNAKE_BODY;

        }

        return self.COLORS.BOARD;

      };


      
      function update() {
      
        var newHead = self.getNewHead();



        if (self.boardCollision(newHead) || self.selfCollision(newHead)) {

          return self.gameOver();

        } else if (self.fruitCollision(newHead)) {

          self.eatFruit();

        }



        // Remove tail

        var oldTail = self.snake.parts.pop();

        self.board[oldTail.y][oldTail.x] = false;



        // Pop tail to head

        self.snake.parts.unshift(newHead);

        self.board[newHead.y][newHead.x] = true;



        // Do it again

        self.snake.direction = self.tempDirection;

        $timeout(self.update, self.interval);

      }



      function getNewHead() {
     
        var newHead = angular.copy(self.snake.parts[0]);

        console.log("---getnewhead---");
        console.log(newHead);

        // Update Location

        if (self.tempDirection === self.DIRECTIONS.LEFT) {

            newHead.x -= 1;

        } else if (self.tempDirection === self.DIRECTIONS.RIGHT) {

            newHead.x += 1;

        } else if (self.tempDirection === self.DIRECTIONS.UP) {

            newHead.y -= 1;

        } else if (self.tempDirection === self.DIRECTIONS.DOWN) {

            newHead.y += 1;

        }

        console.log("---getnewhead 2---");
        console.log(newHead);

        return newHead;

      }



      //
      function boardCollision (part) {

        return part.x === self.BOARD_SIZE || part.x === -1 || part.y === self.BOARD_SIZE || part.y === -1;

      }

      

      function selfCollision(part) {
     
        return self.board[part.y][part.x] === true;

      }



      function fruitCollision(part) {
        return part.x === self.fruit.x && part.y === self.fruit.y;

      }



      function resetFruit() {
     
        var x = Math.floor(Math.random() * self.BOARD_SIZE);

        var y = Math.floor(Math.random() * self.BOARD_SIZE);



        if (self.board[y][x] === true) {

          console.log("-----1---")
          console.log(self.fruit);
          return self.resetFruit();

         

        }

        self.fruit = {x: x, y: y};
        console.log("-----2---")
        console.log(self.fruit);
      }

      
      function eatFruit() {
      
        self.score++;

        

        // Grow by 1

        var tail = angular.copy(self.snake.parts[self.snake.parts.length-1]);

        self.snake.parts.push(tail);

        self.resetFruit();



        if (self.score % 5 === 0) {

          self.interval -= 15;

        }

      }

      function gameOver() {
        self.isGameOver = true;



        $timeout(function() {

          self.isGameOver = false;

        },500);



        self.setupBoard();

      }



      function setupBoard() {
     
        self.board = [];

        for (var i = 0; i < self.BOARD_SIZE; i++) {

          self.board[i] = [];

          for (var j = 0; j < self.BOARD_SIZE; j++) {

            self.board[i][j] = false;

          }

        }

      }

      self.setupBoard();



      $window.addEventListener("keyup", function(e) {

        if (e.keyCode == self.DIRECTIONS.LEFT && self.snake.direction !== self.DIRECTIONS.RIGHT) {

          self.tempDirection = self.DIRECTIONS.LEFT;

        } else if (e.keyCode == self.DIRECTIONS.UP && self.snake.direction !== self.DIRECTIONS.DOWN) {

          self.tempDirection = self.DIRECTIONS.UP;

        } else if (e.keyCode == self.DIRECTIONS.RIGHT && self.snake.direction !== self.DIRECTIONS.LEFT) {

          self.tempDirection = self.DIRECTIONS.RIGHT;

        } else if (e.keyCode == self.DIRECTIONS.DOWN && self.snake.direction !== self.DIRECTIONS.UP) {

          self.tempDirection = self.DIRECTIONS.DOWN;

        }

      });



      self.startGame = function() {

        self.score = 0;
        console.log(self.score);

        self.snake = {direction: self.DIRECTIONS.LEFT, parts: []};
        console.log(self.snake);

        self.tempDirection = self.DIRECTIONS.LEFT;
        console.log(self.tempDirection);
        self.isGameOver = false;
        console.log(self.isGameOver);
        self.interval = 150;
        console.log(self.interval);


        // Set up initial snake

        for (var i = 0; i < 5; i++) {

          self.snake.parts.push({x: 10 + i, y: 10});

        }

        self.resetFruit();

        self.update();

      };



  }

})();